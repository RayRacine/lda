module Corpus (
  buildCorpus,
  corpusStats,
  vocabularyWordCount
) where

import           Debug.Trace

import           Document
import           Types                       (Corpus (..), CorpusStats (..),
                                              DocAssignement (..), DocText,
                                              Ident, Wid, Word,
                                              WordAssignment (..), WordIds (..))

import           Control.Monad               (foldM)
import           Control.Monad.Primitive     (PrimState)
import           Control.Monad.ST
import qualified Data.HashMap.Strict         as HM
import           Data.Vector.Unboxed         (length, maximum, unsafeFreeze)
import           Data.Vector.Unboxed.Mutable (MVector, new, unsafeWrite)
import           Prelude                     hiding (length, maximum, words)

corpusStats :: Corpus -> Int -> CorpusStats
corpusStats corpus topics  =
  let DocAssignement dids = cDid corpus
      WordAssignment wids = cWid corpus
  in  CorpusStats (length wids)
                  ((maximum dids) + 1) -- RPR - dangerous assumes starts at Doc 0 and is continous
                  (vocabularyWordCount corpus)
                  topics

type MAssignmentArray s = MVector (PrimState (ST s)) Ident

data CorpusAccum s = CorpusAccum WordIds Int (MAssignmentArray s) (MAssignmentArray s)

wordIds :: WordIds
wordIds = WordIds HM.empty 0

-- corpusLength :: Corpus -> Int
-- corpusLength (Corpus _ (WordAssignment wd) _ ) =
--               length wd

vocabularyWordCount :: Corpus -> Int
vocabularyWordCount corpus = let WordIds wMap _ = (cWids corpus)
                             in HM.size wMap

widForWord :: WordIds -> Word -> (WordIds , Wid)
widForWord wids@(WordIds widMap lastWid) word =
  case HM.lookup word widMap of
   Just wid -> (wids, wid)
   Nothing ->
     let lastWid' = lastWid + 1
         widMap' = HM.insert word lastWid widMap
     in (WordIds widMap' lastWid', lastWid)

parseDocs :: [DocText] -> [Document]
parseDocs docs =
  map (\ (doc, did) -> parseDocument doc did) (zip docs  [0..])

processDoc :: CorpusAccum s -> Document -> ST s (CorpusAccum s)
processDoc (CorpusAccum cWidIds ith wids dids) (Document did dwords) = do
  processWords cWidIds dwords ith
  where
    processWords widIds [] i = do return $ CorpusAccum widIds i wids dids
    processWords widIds (word : words) i = do
      let (widIds', wid) = widForWord widIds word
      unsafeWrite wids i wid
      unsafeWrite dids i did
      processWords widIds' words (i + 1)

buildCorpus :: [DocText] -> Corpus
buildCorpus docs =
  let pDocs  = parseDocs docs
      len = docsLength (traceShow pDocs pDocs)
  in
   runST $ do
     wids <- new len
     dids <- new len
     let cAccum = CorpusAccum wordIds 0 wids dids
     CorpusAccum widMap _ wids' dids' <- foldM processDoc cAccum pDocs
     fWids <- unsafeFreeze wids'
     fDids <- unsafeFreeze dids'
     return $ Corpus widMap (WordAssignment fWids) (DocAssignement fDids)
