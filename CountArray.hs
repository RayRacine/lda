module CountArray where

import           Control.Monad.Primitive     (PrimState)
import           Control.Monad.ST
import           Data.Vector.Unboxed         (Vector, unsafeFreeze, unsafeIndex,
                                              unsafeThaw)
import           Data.Vector.Unboxed.Mutable (MVector, length, new, unsafeRead,
                                              unsafeWrite)
import           Prelude                     hiding (length, read, span)

-- Word Count
type Count = Int

newtype N = N Count

-- An array of probabilities.
type Probability = Double
type ProbArray   = Vector Double
type MProbArray s = MVector (PrimState (ST s)) Double

type CountArray    = Vector Count
type MCountArray s = MVector (PrimState (ST s)) Count

newtype CountArray2D  = CountArray2D CountArray
        deriving Show
data MCountArray2D s  = MCountArray2D (MCountArray s)

-- Count Arrays

indexCountArray :: Vector Count -> Int -> Count
indexCountArray v i =
  unsafeIndex v i

countArray :: Int -> CountArray
countArray sz = runST $ do
  v <- new sz
  unsafeFreeze v

countArray2D :: Int -> Int -> CountArray2D
countArray2D rows cols =
  CountArray2D (countArray (rows * cols))

incCountArray :: MCountArray s -> Int -> ST s ()
incCountArray v i = do
  x  <- unsafeRead v i
  unsafeWrite v i (x+1)

decCountArray :: MCountArray s -> Int -> ST s ()
decCountArray v i = do
  x <- unsafeRead v i
  unsafeWrite v i (x - 1)

-- Count Array2D
index :: Int -> Int -> Int -> Int
index span r c =
  span * r + c

unsafeThaw2D :: CountArray2D -> ST s (MCountArray2D s)
unsafeThaw2D (CountArray2D v) = do
  v' <- unsafeThaw v
  return $ MCountArray2D v'

length2D :: MCountArray2D s -> ST s Int
length2D (MCountArray2D v) =
  return $ length v

unsafeFreeze2D :: MCountArray2D s -> ST s CountArray2D
unsafeFreeze2D (MCountArray2D v) = do
  v' <- unsafeFreeze v
  return $ CountArray2D v'

read2D :: CountArray2D -> Int -> Int -> Int -> Int
read2D (CountArray2D v) s r c =
  unsafeIndex v (index s r c)

unsafeRead2D :: MCountArray2D s -> Int -> Int -> Int -> ST s Int
unsafeRead2D (MCountArray2D v) s r c = do
  unsafeRead v (index s r c)

unsafeWrite2D :: MCountArray2D s -> Int -> Int -> Int -> Int -> ST s ()
unsafeWrite2D (MCountArray2D v) s r c x = do
  unsafeWrite v (index s r c) x

unsafeModify2D :: MCountArray2D s -> Int -> Int -> Int -> (Int -> Int) -> ST s ()
unsafeModify2D (MCountArray2D v) s r c f = do
  let i = index s r c
  x <- unsafeRead v i
  unsafeWrite v i (f x)

incCountArray2D :: MCountArray2D s -> Int -> Int -> Int -> ST s ()
incCountArray2D v s r c =
  unsafeModify2D v s r c (+ 1)

decCountArray2D :: MCountArray2D s -> Int -> Int -> Int -> ST s ()
decCountArray2D v s r c = unsafeModify2D v s r c (+ (-1))

dec :: Int -> Int
dec i = i - 1
