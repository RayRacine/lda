module Types (
  LDAPrior (..),
  Corpus (..),
  CorpusStats (..),
  DocAssignement (..),
  WordAssignment (..),
  Iters (..),
  Topic (..),
  Did, Wid,
  DocText, Word,
  Ident,
  WordIds (..)
) where

import           CountArray
import qualified Data.HashMap.Strict as HM
import qualified Data.Text           as T
import           Data.Vector.Unboxed (Vector)

-- Topic ID
newtype Topic = Topic Count
newtype Iters = Iters Count

type Ident = Int
type AssignmentArray = Vector Ident

type Wid = Ident
type Did = Ident
type Word = T.Text
type DocText = T.Text
data WordIds = WordIds (HM.HashMap Word Wid) Int
     deriving Show

newtype WordAssignment = WordAssignment AssignmentArray
        deriving Show
newtype DocAssignement = DocAssignement AssignmentArray
        deriving Show

data Corpus = Corpus { cWids :: WordIds,
                       cWid  :: WordAssignment,
                       cDid  :: DocAssignement }
              deriving Show

data CorpusStats = CorpusStats { csWordCount       :: Int,
                                 csDocCount        :: Int,
                                 csVocabularyCount :: Int,
                                 csTopicCount      :: Int }
                   deriving Show

data LDAPrior = LDAPrior { ldapAlpha :: Double,
                           ldapBeta  :: Double,
                           ldapWBeta :: Double } deriving Show

-- data LDA =
--   LDA {  -- Word Id of the nth word
--         ldaWid :: CountArray, -- wid(N)
--         -- Document Id of the nth word
--         ldaDid :: CountArray, -- did(N)
--          -- topic assignment of the nth word
--         ldaZ   :: CountArray, -- Z(N)
--         -- Count of word w in topic t
--         ldaCwt :: CountArray2D, -- W x T
--         -- Count of topic t in document d
--         ldaCdt :: CountArray2D, -- D x T
--         -- Count of topic t
--         ldaCt  :: CountArray }  -- ct(T)
--   deriving Show
