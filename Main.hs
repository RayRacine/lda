{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}


import           Corpus (buildCorpus, vocabularyWordCount)
import           Lda
import           Types

docs :: [DocText]
docs = ["Cat Dog Mouse",
        "Cat Dog Cow",
        "Cat Dog Bird",
        "Bird Cow Mouse Cat Dog",
        "Cat Dog Mouse Bird",
        "Ray Cory Eve",
        "Cory Eve",
        "Ray Eve"]

docs2 :: [DocText]
docs2 = [ "eat broccoli banana",
          "eat  banana spinach smoothie breakfast",
          "Chinchilla kitten cute",
          "Sister adopted kitten yesterday",
          "Look cute hamster munching piece broccoli" ]

main :: IO ()
main = do
  let corpus = buildCorpus docs
      priors = LDAPrior alpha beta ((fromIntegral . vocabularyWordCount)  corpus * beta)
  putStrLn $ show corpus
  lda' <- runLda priors corpus (Topic 5) (Iters 1000000)
  print $ show lda'
  return ()
  where
    alpha = 0.05
    beta = 0.01
