module Document
       where

import           Types

import qualified Data.Text as T

data Document = Document Did [Word]
     deriving Show

parseDocument :: T.Text -> Did -> Document
parseDocument text did =
  Document did (splitSpace  text)
  where
    splitSpace = T.split (\c -> c == ' ')

docLength :: Document -> Int
docLength (Document _ tokens) = length tokens

docsLength :: [Document] -> Int
docsLength docs = sum (map docLength docs)

