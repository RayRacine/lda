module Lda (
  runLda
) where

import           Corpus
import           CountArray
import           Types                       (Corpus (..), CorpusStats (..),
                                              DocAssignement (..),
                                              Iters (Iters),
                                              LDAPrior (LDAPrior),
                                              Topic (Topic),
                                              WordAssignment (..))

import           Control.Monad               (foldM)
import           Control.Monad.ST
import           Data.Vector.Unboxed         (sum, unsafeFreeze, unsafeIndex,
                                              unsafeThaw, (!))
import           Data.Vector.Unboxed.Mutable (new, read, unsafeWrite, write)
import           Prelude                     hiding (read, sum)
import           System.Random.MWC           (Gen, createSystemRandom, uniformR)

data LdaST s = LdaST { ldastZ   :: MCountArray s,
                       ldastCwt :: MCountArray2D s,
                       ldastCdt :: MCountArray2D s,
                       ldastCt  :: MCountArray s }

data Lda = Lda { ldaZ   :: CountArray,    -- topic assignment of the nth corpus word
                 ldaCwt :: CountArray2D,  -- count of vocabulary work w in topic t. WxT
                 ldaCdt :: CountArray2D,  -- count of topic t in document d, DxT
                 ldaCt  :: CountArray }   -- count of corpus words in topic t.
           deriving Show

mkLda :: CorpusStats -> Lda
mkLda (CorpusStats wc dc vc tc) =
  Lda (countArray wc)
      (countArray2D vc tc)
      (countArray2D tc dc)
      (countArray tc)

ldaST :: Lda -> ST s (LdaST s)
ldaST (Lda z cwt cdt ct) = do
  zST   <- unsafeThaw z
  cwtST <- unsafeThaw2D cwt
  cdtST <- unsafeThaw2D cdt
  ctST  <- unsafeThaw ct
  return $ LdaST zST cwtST cdtST ctST

unsafeFreezeLda :: LdaST s -> ST s Lda
unsafeFreezeLda (LdaST stZ stCwt stCdt stCt) = do
  z <- unsafeFreeze stZ
  cwt <- unsafeFreeze2D stCwt
  ctd <- unsafeFreeze2D stCdt
  ct <- unsafeFreeze stCt
  return $ Lda z cwt ctd ct

topicProbArray :: Int -> ProbArray
topicProbArray t = runST $ do
  v <- new t
  unsafeFreeze v

currentWordIndex :: WordAssignment -> Int -> Int
currentWordIndex (WordAssignment wids) i =
  unsafeIndex wids i

currentDocIndex :: DocAssignement -> Int -> Int
currentDocIndex (DocAssignement dids) i =
  unsafeIndex dids i

-- Create an initial Count Array
-- Assign a random Topic from 0 to T-1
randomTopicAssign :: Gen s -> CorpusStats -> Corpus -> LdaST s -> ST s Lda
randomTopicAssign gen (CorpusStats wc dc _ tc) (Corpus _ wids dids) l@(LdaST z cwt cdt ct) = do
  _ <- foldM assignTopics l [0 .. wc - 1]
  unsafeFreezeLda l
    where
      tmax = tc - 1
      assignTopics _ i = do
        let wi = currentWordIndex wids i
            di = currentDocIndex dids i
        ti <- uniformR (0, tmax) gen      -- random topic
        unsafeWrite z i ti               -- assign tokens topic
        incCountArray2D cwt tc wi ti
        incCountArray2D cdt tc di ti
        incCountArray ct ti
        return l

-- Sample Topic Probability
{- Note the topic probabilities are not normalized.
   Therefore we uniformly sample across their sum and find the topic in the interval.-}
sampleTopic :: Gen s -> ProbArray -> ST s Int
sampleTopic randGen probs = do
  let ptot = sum probs
  r <- uniformR (0, ptot) randGen
  let paccum = probs ! 0
      topic  = sample r paccum (Topic 0)
  return topic
  where
    sample r paccum (Topic t) =
      if r > paccum
      then let t' = t + 1
               paccum' = paccum + (probs ! t')
           in sample r paccum' (Topic t')
      else t

-- Execute a single pass over the entire corpus
corpusTopicPosteriors :: LDAPrior -> CorpusStats -> Corpus -> Gen s -> MProbArray s -> LdaST s -> ST s Lda
corpusTopicPosteriors (LDAPrior alpha beta wbeta)
                      (CorpusStats wc dc _ tc)
                      (Corpus _ wid did) genR tps ldaSt = do
  l <- foldM wordTopicPosteriors ldaSt [0 .. wc - 1]
  unsafeFreezeLda l
  where
    wordTopicPosteriors (LdaST z cwt cdt ct) i = do -- i index of corpus word
      let wi = currentWordIndex wid i
          di = currentDocIndex did i
      ti <- currentTopicAssignment
      decCt ti
      decCwt wi ti
      decCdt di ti     -- backout the current corpus word token count
      ti' <- topicAssignment wi di
      assignTopic z ti'
      incCwt wi ti'
      incCdt di ti'
      incCt ti' -- update counts given the new corpus word token topic
      return $ LdaST z cwt cdt ct
       where
         assignTopic z' t            = write z' i t
         currentTopicAssignment      = do read z i
         decCt ti                    = decCountArray ct ti
         decCwt wi ti                = decCountArray2D cwt tc wi ti
         decCdt di ti                = decCountArray2D cdt tc di ti
         incCwt wi ti                = incCountArray2D cwt tc wi ti
         incCdt di ti                = incCountArray2D cdt tc di ti
         incCt ti                    = incCountArray ct ti
         topicAssignment wi di    = do
            tps' <- topicProbs cwt cdt wi di tps (dec tc)
            sampleTopic genR tps'
         topicProbs cwt' ctd' wi di probs ti =
             if ti < 0
             then unsafeFreeze probs
             else do wt <- unsafeRead2D cwt' tc wi ti
                     zt <- read z ti
                     dt <- unsafeRead2D ctd' dc ti di
                     let zti = fromIntegral zt
                         dti = fromIntegral dt
                         wti = fromIntegral wt
                         p   = (wti + beta) / (zti + wbeta * (dti + alpha))
                     write probs ti p
                     topicProbs cwt' ctd' wi di probs (dec ti)

runPass :: LDAPrior -> CorpusStats -> Corpus -> Gen RealWorld -> ProbArray -> Lda -> IO Lda
runPass priors stats corpus genR probArray ldaPass = do
  stToIO $ do
    probArray' <- unsafeThaw probArray
    ldaSt <- ldaST ldaPass
    corpusTopicPosteriors priors stats corpus genR probArray' ldaSt

runLda :: LDAPrior -> Corpus -> Topic -> Iters -> IO Lda
runLda priors corpus (Topic topics) (Iters iters) = do
  genR <- createSystemRandom
  print "Corpus Stats"
  print $ show stats
  print "Creating Lda"
  let lda' = mkLda stats
  print $ show lda'
  print "Assigning random topics"
  lda'' <- stToIO $ do
    ldaSt <- ldaST lda'
    randomTopicAssign genR stats corpus ldaSt
  print $ show lda''
  print $ "Running iterations: " ++ show iters
  runIter genR lda'' 0
  where
    stats = corpusStats corpus topics
    probArray = topicProbArray topics
    runIter genR ldaPass i | i < iters = do
      ldaPass' <- runPass priors stats corpus genR probArray ldaPass
      runIter genR ldaPass' (i + 1)
    runIter _ ldaPass _ =  return ldaPass
